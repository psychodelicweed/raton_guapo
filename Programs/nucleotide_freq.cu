
#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <time.h>
#include <dirent.h>
#define SEQLEN 1271485
#define SAMPLES 30
#define BLOCKSX 1024
#define BLOCKSY 625
#define THREADSX 2
#define STARTPOS 34132957

typedef struct nucleotide
{
	int pos;
	int A;
	int C;
	int G;
	int T;
} Nucleotide;



//Prototipos
__global__ void cudaNucleotide(char *sequence, Nucleotide *nucleotides);
void openFiles(char *sequence, char * input_folder);


int main(int argc, char *argv[])
{
	//Creando los apuntadores en cpu
	Nucleotide *result;
	char *full_seq;

	//Apuntadores que nos daran la direccion en memoria ocupada de la gpu
	//Nucleotide *dev_input_struct;
	Nucleotide *dev_result;
	char *dev_full_seq;

	//Reservar memoria necesaria,
	result=(Nucleotide*)malloc( sizeof(Nucleotide) * SEQLEN ); //Para guardar el resultado de la gpu

	//La secuencia completa por el nunerobde muestras
	full_seq=(char *)malloc( sizeof(char) * (( 1 + SEQLEN) * SAMPLES) );

	//Abrir los 30 archivos y guardalos
	openFiles(full_seq, argv[1]);


//CUDA CODE STARTS HERE:
//Paso 1, reservar la memoria necesaria para el GPU, en este ejemplo vamos a transferir ~50MB
//Estructuras donde guardara los datos
	//cudaMalloc(&dev_input_struct, sizeof(Nucleotide) * SEQLEN);
	cudaMalloc(&dev_result, sizeof(Nucleotide) * SEQLEN);

//Super arreglo con las secuencias
	cudaMalloc(&dev_full_seq, sizeof(char) * (( 1 + SEQLEN) * SAMPLES));

//Paso 2 llenamos con 0s
	cudaMemset(dev_result, 0, sizeof(Nucleotide) * SEQLEN );
	cudaMemset(&dev_full_seq, 0, sizeof(char) * (( 1 + SEQLEN) * SAMPLES));

//Paso 3, Copiar la secuencia en la memoria del GPU
	cudaMemcpy(dev_full_seq, full_seq, (sizeof(char) * (( 1 + SEQLEN) * SAMPLES)), cudaMemcpyHostToDevice);

//Paso 4
	//Declarar los bloques que usaremos, hilos=THREADSX
	const dim3 BLKS(BLOCKSX, BLOCKSY);

//Paso 5, INVOCAMOS EL KERNEL!!!
	cudaNucleotide<<<BLKS,THREADSX>>>(dev_full_seq, dev_result);

//Paso 6, Recuperamos la informacion
	cudaMemcpy(result, dev_result, sizeof(Nucleotide) * SEQLEN, cudaMemcpyDeviceToHost);
//Paso 7
	cudaFree(dev_result);
	cudaFree(dev_full_seq);
//CUDA CODE ENDS HERE;

//Imprimimos resultados

	//Iteradores, i->Va recorrer las muestras	j->El tamaño de la secuencia
	int i;

	printf("POS\tA\tG\tC\tT\tFRQA\tFRQG\tFRQC\tFRQT\n");
	for(i=0; i<SEQLEN; i++)
	{
		int pos=result[i].pos;
		int A=result[i].A;
		int C=result[i].C;
		int G=result[i].G;
		int T=result[i].T;
		printf("%d\t%d\t%d\t%d\t%d\t%.2f\t%.2f\t%.2f\t%.2f\n",(pos+STARTPOS),A,C,G,T,(float)A/SAMPLES,(float)C/SAMPLES,(float)G/SAMPLES,(float)T/SAMPLES);
	}
	printf("\n");

	//No olvidemos liberar la memoria
	free(result);
	free(full_seq);

	return 0;
}


void openFiles(char *sequence, char * input_folder)
{
	//The objects we are going to need
	DIR *directory;

	//Data structure for each file in directory
	struct dirent *dir;

	//Try open directory
	directory = opendir(input_folder);

	int flag=0;

	//If its open
	if (directory)
	{
		// Scanning the in directory
		while ((dir = readdir(directory)) != NULL)
		{

			//En caso de que se encuentre con la carpeta actual y la anterios, siguiente
			if (!strcmp (dir->d_name, "."))
				continue;
			if (!strcmp (dir->d_name, ".."))
				continue;

			char *tmpFile=dir->d_name;
			char *file_open=(char *) malloc(1 + strlen(input_folder)+ strlen(tmpFile) );
			strcpy(file_open, input_folder);
			strcat(file_open, tmpFile);

			//Abrir Archivo
			FILE *file=fopen(file_open, "r");
			//printf("Archivo:%s\n",dir->d_name);

			//Tamano de la linea
			char *line=(char *)malloc(sizeof(char) * (SEQLEN + 1));

			fgets(line, SEQLEN+1, file);
			//printf("%s\n",line);

			if(flag==0)
			{
				strcpy(sequence,line);
				flag=1;
			}
			else
			{
				strcat(sequence,line);
			}

			//strcpy(nextPicture, input_folder);
			free(line);
		}
		closedir(directory);
	}

}



/////////////////////////////////7
//
/*
	CUDA KERNEL HERE
		:D

//////////////////////////////////
*/
__global__ void cudaNucleotide(char *sequence, Nucleotide *nucleotides)
{
	//Get identifiers for each block and thread 2D
	int blkX=blockIdx.x;	//1024
	int blkY=blockIdx.y;	//625
	int thrdX=threadIdx.x;	//2

	//Get the index
	int index=( ( (blkX * BLOCKSY) + blkY ) * THREADSX) + thrdX;

	//Declaramos i, i-> cada muestra de raton :P
	int i;
	//Si el hilo se encuentra dentro del rango :D
	if( index < SEQLEN )
	{
		for(i=0; i<SAMPLES; i++)
		{
			//Calculamos el indice en la posicion de cada archivo i
			int myIndex=index+(i*SEQLEN);
			//Si el campo no esta vacio
			if(sequence[myIndex] != '-')
			{
				//Guardar en el caso de cualquier nucleotido
				switch(sequence[myIndex])
				{
					case 'A':
						nucleotides[index].A+=1;
						break;

					case 'C':
						nucleotides[index].C+=1;
						break;
					case 'G':
						nucleotides[index].G+=1;
						break;
					case 'T':
						nucleotides[index].T+=1;
						break;
					default:
						break;

				}
			}
		}
		//Guardar la posicion en el objeto
		nucleotides[index].pos=index;
	}

}

