## Autor: Ao Kin Ecab Gutiérrez González Loyola ##
# Este programa muestra dos funciones graficadas mediante pyplot

# For installing matplot lib

# Debian / Ubuntu : sudo apt-get install python-matplotlib
# Fedora / Redhat : sudo yum install python-matplotlib
# Windows Enviroment: python -m pip install -U pip setuptools; python -m pip install matplotlib



from matplotlib import pyplot as plt
ycord = []
xcord = []

diff_pos = open(r"C:\Users\Alquimerico\PycharmProjects\raton_guapo\Results\Diff_pos.freq")
data = diff_pos.readlines()
for d in data:
    #print(int(d[:8]))
    #d[9:] frequency number
    if int(d[9:]) !=  0:
        ycord.append(int(d[9:]))
        xcord.append(int(d[:8]))

plt.plot(xcord,ycord)
#print(xcord)
#print(ycord)
plt.xlabel("Posicion")
plt.ylabel("Frecuencia")
plt.title("Frecuencia por posicion")
plt.show() #Muestra la gráfica